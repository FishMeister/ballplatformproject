﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour
{

    public float movingPlatformSpeed;

    public enum ControlType
    {
        Keyboard, 
        Mouse,

        // ! Implement mobile controls later? //
//      Accelerometer,
//      TouchButtons
    }

    public ControlType controlType;

    private PlayerMotor motor;
    
    // self-explainatory lol
    public void OnSensitivityValueChange(float value)
    {
        movingPlatformSpeed = value;
    }
    void Awake()
    {
        // component caching
        motor = this.GetComponent<PlayerMotor>();
    }
	
    void Update()
    {
        float _xMov = 0f;
        //Calculate movement velocity as a 3D vector depending of our control type
        if (controlType == ControlType.Keyboard)
        {
            _xMov = Input.GetAxis("Horizontal");
        }
        if (controlType == ControlType.Mouse)
        {
            _xMov = Input.GetAxis("Mouse X");
        }
        Vector3 _movHorizontal = transform.right * _xMov;

        // Final movement vector
        Vector3 _velocity = _movHorizontal * movingPlatformSpeed;
        //Apply movement
        motor.Move(_velocity);
    }
}
