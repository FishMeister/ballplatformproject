﻿using UnityEngine;
using System.Collections;

public class ParticleDestroy : MonoBehaviour
{
    private float DestroyTimer = 3f, Timer = 0f;

	void Update ()
    {
        Timer += Time.deltaTime;

        if (Timer >= DestroyTimer)
        {
            Destroy(this.gameObject);
        }
	}
}
