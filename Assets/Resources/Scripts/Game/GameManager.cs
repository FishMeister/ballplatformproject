﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    [SerializeField]
    private float Overtime = 30f;

    private float Timer = 0f;

    int score, highscore;

    public PlayerController PC;
    private bool HighscoreBeaten;
    public GameObject GameAtlas, PauseAtlas, LossAtlas, BackgroundColor, InputButton;
    public Text HighscoreLabel, ScoreLabel;
    public float BallSpeed;
    public float BallXPositionRange;
    private enum GameState
    {
        Normal,
        Pause,
        Loss
    }
    [SerializeField]
    private GameState gameState;

    public void InputTypeChange()
    {
        if (PC.controlType == PlayerController.ControlType.Keyboard)
        {
            PC.controlType = PlayerController.ControlType.Mouse;
            InputButton.gameObject.GetComponentInChildren<Text>().text = "Input Type: Mouse";
        }

        else
        {
            PC.controlType = PlayerController.ControlType.Keyboard;
            InputButton.gameObject.GetComponentInChildren<Text>().text = "Input Type: Keyboard";
        }

    }
    void Awake()
    {
     //   PlayerPrefs.DeleteAll();
        Time.timeScale = 1;
        highscore = PlayerPrefs.GetInt("highscore");
        HighscoreLabel.text = highscore.ToString();
    }
    public void MainMenu()
    {
        Application.LoadLevel(0);
    }
    public void OnRestart()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
    public void ScoreIncrease()
    {
        score++;
        Debug.Log(score);
    }
    public void Pause()
    {
        DifficultyIncrease();

        PauseAtlas.SetActive(true); GameAtlas.SetActive(false); BackgroundColor.SetActive(true);
        gameState = GameState.Pause;
        Time.timeScale = 0f;
        
    }
    public void Unpause()
    {
        BackgroundColor.SetActive(false);
        PauseAtlas.SetActive(false);
        GameAtlas.SetActive(true);
        gameState = GameState.Normal;
        Time.timeScale = 1f;
    }
    public void Loss()
    {
        GameAtlas.SetActive(false); PauseAtlas.SetActive(false); LossAtlas.SetActive(true); BackgroundColor.SetActive(true);
       
        if (HighscoreBeaten)
            Congrats();

        gameState = GameState.Loss;
       
        Time.timeScale = 0;
    }

    void Congrats()
    {

    }
    void FixedUpdate()
    {
        if (gameState == GameState.Normal)
        {
            ScoreLabel.text = score.ToString();
            if (score > highscore)
            {
                highscore = score;

                PlayerPrefs.SetInt("highscore", score);
                HighscoreLabel.text = highscore.ToString();
                HighscoreBeaten = true;
               
            }
            // setting timer for difficulty to linear increase 
            Timer += Time.fixedDeltaTime;
            if (Timer >= Overtime)
            {
                DifficultyIncrease();
                Timer = 0f;
            }
        }
    }

    void DifficultyIncrease()
    {
        BallSpeed -= 1.5f;
    }
}
