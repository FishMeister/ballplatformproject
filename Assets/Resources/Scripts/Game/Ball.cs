﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{
    private GameManager GM;
    private Vector3 InitialPosition;
    public GameObject CollisionParticle;
    float y = 0;
    float x = 0;
    float z = 1;
    Vector3 mov;

    public void Awake()
    {
        if (GM == null)
        {
            GM = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
        }
        InitialPosition = this.transform.position;
        ResetPosition();
    }

    public void OnCollisionEnter(Collision col)
    {
        if (col.collider.tag == "Player")
        {
            // Spawn particle system
            ContactPoint contact = col.contacts[0];
            Vector3 _pos = contact.point;
            Quaternion _rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
            GameObject _CollisionParticle = Instantiate(CollisionParticle, _pos, _rot) as GameObject;
            _CollisionParticle.transform.parent = col.transform;
      

            GM.ScoreIncrease();
            ResetPosition();
        }
        if (col.collider.tag == "Killzone")
        {
            GM.Loss();
        }
        if (col.collider.tag == "Out")
        {
            ResetPosition();
        }
    }

 

  
    void ResetPosition()
    {
        this.GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);
        this.GetComponent<Rigidbody>().angularVelocity = new Vector3(0f, 0f, 0f);
  

        float _RandomXPosition = Random.Range(-GM.BallXPositionRange, GM.BallXPositionRange);
        Vector3 _pos = new Vector3(_RandomXPosition, InitialPosition.y, InitialPosition.z);
        this.transform.position = _pos;

        Vector3 _force = new Vector3(Random.Range(-25f, 25f), GM.BallSpeed, 0);
        this.GetComponent<Rigidbody>().AddForce(_force, ForceMode.Impulse);
    }
    
	
}
