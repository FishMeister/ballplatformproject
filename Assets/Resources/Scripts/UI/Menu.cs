﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Menu : MonoBehaviour {

    [SerializeField]
    private Text HighscoreLabel;

    public void Start()
    {
        int _highscore = PlayerPrefs.GetInt("highscore");
        HighscoreLabel.text = _highscore.ToString();
    }

	public void ExitApplication ()
    {
        Application.Quit();
	}
	
	
	public void Play ()
    {
        Application.LoadLevel(1);
	}


}
